<?php

namespace booking\Repository;

/**
 * Class SlaveRepository
 * @package booking\Repository
 */
class AbstractRepository
{
    protected $storage = [];

    /**
     * @param $id
     * @return mixed
     * @throws \ErrorException
     */
    public function get($id) : object
    {
        if (!isset($this->storage[$id])){
            throw new \ErrorException(sprintf('Entity ' . static::getEntityClass() . ' with ' . $id . ' not found'));
        }

        return $this->storage[$id];
    }

    /**
     * @param $object
     */
    public function add($object) : void
    {
        $identity = $this->getIdentity($object);

        $this->storage[$this->getEntityClass()][$identity] = $object;
    }

    /**
     * @param object $object
     * @return string
     */
    protected function getIdentity(object $object) : string
    {
        return spl_object_hash($object);
    }

    /**
     * @return string
     */
    abstract protected function getEntityClass() : string;
}