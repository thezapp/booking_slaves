<?php

namespace booking\Repository;

use booking\Entity\Slave\Slave;

/**
 * Class BookingRepository
 * @package booking\Repository
 */
class SlaveRepository extends AbstractRepository
{
    /**
     * @return string
     */
    protected function getEntityClass() : string
    {
        return Slave::class;
    }
}