<?php

namespace booking\Repository;

use booking\Entity\Tenant;

/**
 * Class BookingRepository
 * @package booking\Repository
 */
class TenantRepository extends AbstractRepository
{
    /**
     * @return string
     */
    protected function getEntityClass() : string
    {
        return Tenant::class;
    }
}