<?php

namespace booking\Services;

use booking\Dto\Booking;
use booking\Entity\Period;
use booking\Entity\Slave\Slave;
use booking\Entity\Tenant;
use booking\Repository\SlaveRepository;
use booking\Repository\TenantRepository;

/**
 * Class BookingService
 * @package booking\Services
 */
class BookingService
{
    /**
     * @var SlaveRepository
     */
    protected $slaveRepository;

    /**
     * @var TenantRepository
     */
    protected $tenantRepository;

    /**
     * BookingService constructor.
     * @param SlaveRepository $slaveRepository
     * @param TenantRepository $tenantRepository
     */
    public function __construct(SlaveRepository $slaveRepository, TenantRepository $tenantRepository)
    {
        $this->slaveRepository = $slaveRepository;

        $this->tenantRepository = $tenantRepository;
    }

    /**
     * @param Booking $booking
     * @throws \ErrorException
     */
    public function rent(Booking $booking) : void
    {
        /** @var Tenant $tenant */
        $tenant = $this->slaveRepository->get($booking->tenantId);

        /** @var Slave $slave */
        $slave = $this->slaveRepository->get($booking->slaveId);

        $slave->book($tenant, new Period($booking->dateFrom, $booking->dateTo));
    }
}