<?php

namespace booking\Entity;

/**
 * Class Booking
 * @package booking\Entity
 */
class Booking
{
    /**
     * @var Period
     */
    protected $period;

    /**
     * @var Tenant
     */
    protected $tenant;

    /**
     * @var integer
     */
    protected $status;

    /**
     * @var string
     */
    protected $reason;

    const BOOKING_OPENED = 1;

    const BOOKING_COMPLETED = 2;

    const BOOKING_CANCELED = 3;

    /**
     * Booking constructor.
     * @param Period $period
     * @param Tenant $tenant
     */
    public function __construct(Period $period, Tenant $tenant)
    {
        $this->period = $period;

        $this->tenant = $tenant;

        $this->status = self::BOOKING_OPENED;
    }

    /**
     * @return Period
     */
    public function getPeriod() : Period
    {
        return $this->period;
    }

    /**
     * @return Tenant
     */
    public function getTenant() : Tenant
    {
        return $this->tenant;
    }

    /**
     * @return Tenant
     */
    public function getReason() : string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @throws \ErrorException
     */
    public function cancel(string $reason = '') : void
    {
        if ($this->isCanceled()){
            throw new \ErrorException('Бронь уже отменена');
        }

        $this->status = self::BOOKING_CANCELED;
        $this->reason = $reason;
    }

    /**
     * @return bool
     */
    public function isCanceled() : bool
    {
        return $this->status === self::BOOKING_CANCELED;
    }

    /**
     * @return bool
     */
    public function isOpened() : bool
    {
        return $this->status === self::BOOKING_OPENED;
    }

    /**
     * @param $period
     * @return bool
     */
    public function isBookedPeriod($period) : bool
    {
        return $this->isOpened() && $this->getPeriod()->isIntersect($period);
    }
}