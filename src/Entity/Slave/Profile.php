<?php

namespace booking\Entity\Slave;

/**
 * Class Profile
 * @package booking\Entity\Slave
 */
class Profile
{
    /**
     * @var string
     */
    protected $nickname;

    /**
     * @var string
     */
    protected $gender;

    /**
     * @var int
     */
    protected $age;

    /**
     * @var int
     */
    protected $weight;

    /**
     * @var string
     */
    protected $birthplace;

    /**
     * @var array
     */
    protected $characteristics;

    /**
     * Profile constructor.
     * @param $nickname
     * @param $gender
     * @param $age
     * @param $weight
     * @param $birthplace
     * @param $characteristics
     */
    public function __construct(string $nickname, string $gender, int $age, int $weight, string $birthplace, array $characteristics = [])
    {
        $this->nickname = $nickname;

        $this->gender = $gender;

        $this->age = $age;

        $this->weight = $weight;

        $this->birthplace = $birthplace;

        $this->characteristics = $characteristics;
    }

    /**
     * @return mixed
     */
    public function getNickname() : string
    {
        return $this->nickname;
    }

    /**
     * @return mixed
     */
    public function getGender() : string
    {
        return $this->gender;
    }

    /**
     * @return mixed
     */
    public function getAge() : int
    {
        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getWeight() : int
    {
        return $this->weight;
    }

    /**
     * @return mixed
     */
    public function getBirthplace() : string
    {
        return $this->birthplace;
    }

    /**
     * @return mixed
     */
    public function getCharacteristics() : array
    {
        return $this->characteristics;
    }
}