<?php

namespace booking\Entity\Slave;

use booking\Entity\Booking;
use booking\Entity\Category;
use booking\Entity\Period;
use booking\Entity\Tenant;

/**
 * Class Slave
 * @package booking\Entity
 */
class Slave
{
    /**
     * @var Profile
     */
    protected $profile;

    /**
     * @var int
     */
    protected $hourlyRate;

    /**
     * @var int
     */
    protected $cost;

    /**
     * @var Booking[]
     */
    protected $bookingList = [];

    /**
     * @var Category[]
     */
    protected $categories = [];

    /**
     * Slave constructor.
     * @param Profile $profile
     * @param integer $hourlyRate
     * @param integer $cost
     */
    public function __construct(Profile $profile, int $hourlyRate, int $cost)
    {
        $this->profile = $profile;

        $this->hourlyRate = $hourlyRate;

        $this->cost = $cost;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @return int
     */
    public function getHourlyRate(): int
    {
        return $this->hourlyRate;
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->cost;
    }

    /**
     * @return Booking[]
     */
    public function getBookingList(): array
    {
        return $this->bookingList;
    }

    /**
     * @param Tenant $tenant
     * @param Period $period
     * @throws \ErrorException
     */
    public function book(Tenant $tenant, Period $period) : void
    {
        if ($this->isAlreadyBooked($period)){
            throw new \ErrorException('Раб на указанное время уже арендован');
        }

        $this->bookingList[] = new Booking($period, $tenant);
    }

    /**
     * @param Tenant $tenant
     * @param Period $period
     * @throws \ErrorException
     */
    public function bookByVip(Tenant $tenant, Period $period) : void
    {
        if (!$tenant->isVip()){
            throw new \ErrorException('Пользователь не является VIP-пользователем');
        }

        if ($this->isAlreadyBookedByVip($period)){
            throw new \ErrorException('Раб на указанное время уже арендован другим VIP-пользователем');
        }

        $this->closeIntersectingBooking($period, 'Одна из дат аренды была отдана в пользу VIP-пользователя');

        $this->bookingList[] = new Booking($period, $tenant);
    }

    /**
     * @param Period $period
     * @param string $reason
     * @throws \ErrorException
     */
    public function closeIntersectingBooking(Period $period, string $reason = '') : void
    {
        foreach ($this->bookingList as $booking){
            if ($booking->isBookedPeriod($period)){
                $booking->cancel($reason);
            }
        }
    }

    /**
     * @param Period $period
     * @return bool
     */
    public function isAlreadyBooked(Period $period) : bool
    {
        foreach ($this->bookingList as $booking){
            if ($booking->isBookedPeriod($period)){
                return true;
            }
        }

        return false;
    }

    /**
     * @param Period $period
     * @return bool
     */
    public function isAlreadyBookedByVip(Period $period) : bool
    {
        foreach ($this->bookingList as $booking){
            if ($booking->isBookedPeriod($period) && $booking->getTenant()->isVip()){
                return true;
            }
        }

        return false;
    }
}