<?php


namespace booking\Entity;


use DateInterval;
use DatePeriod;
use DateTime;

class Period
{
    /**
     * @var \DateTimeImmutable
     */
    protected $dateTo;

    /**
     * @var \DateTimeImmutable
     */
    protected $dateFrom;

    const MAX_RENTAL_HOURS = 16;

    /**
     * Period constructor.
     * @param \DateTimeImmutable $dateTo
     * @param \DateTimeImmutable $dateFrom
     * @throws \Exception
     */
    public function __construct(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo)
    {
        $diff = $dateFrom->diff($dateTo);

        if ($diff->d){
            if (iterator_count($this->getPeriod($dateFrom->format('Y-m-d H:i:s'), $dateFrom->format('Y-m-d') . ' 23:59:59')) > self::MAX_RENTAL_HOURS){
                throw new \ErrorException(sprintf('Время аренды первого дня не может быть больше %d часов', self::MAX_RENTAL_HOURS));
            }

            if (iterator_count($this->getPeriod($dateTo->format('Y-m-d' ), $dateTo->format('Y-m-d H:i:s'))) > self::MAX_RENTAL_HOURS){
                throw new \ErrorException(sprintf('Время аренды последнего дня не может быть больше %d часов', self::MAX_RENTAL_HOURS));
            }
        } else {
            if ($diff->h + 1 > self::MAX_RENTAL_HOURS){
                throw new \ErrorException(sprintf('Время аренды не может быть больше %d часов', self::MAX_RENTAL_HOURS));
            }
        }

        $this->dateTo = $dateTo;

        $this->dateFrom = $dateFrom;
    }

    /**
     * @param string $dateFrom
     * @param string $dateTo
     * @return DatePeriod
     * @throws \Exception
     */
    public function getPeriod(string $dateFrom, string $dateTo)
    {
        return new DatePeriod(
            new DateTime($dateFrom),
            new DateInterval('PT1H'),
            new DateTime($dateTo)
        );
    }

    /**
     * @param Period $period
     * @return bool
     */
    public function isIntersect(Period $period) : bool
    {
        return max($this->dateFrom, $period->dateFrom) < min ($this->dateTo, $period->dateTo);
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDateTo(): \DateTimeImmutable
    {
        return $this->dateTo;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDateFrom(): \DateTimeImmutable
    {
        return $this->dateFrom;
    }
}