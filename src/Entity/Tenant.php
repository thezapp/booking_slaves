<?php

namespace booking\Entity;

/**
 * Class Tenant
 * @package booking\Entity
 */
class Tenant
{
    const ROLE_VIP = 'vip';

    const ROL_USER = 'user';

    /**
     * @var array
     */
    protected $roles = [];

    /**
     * Tenant constructor.
     * @param array $roles
     */
    public function __construct(array $roles = [])
    {
        $this->roles = $roles;
    }

    /**
     * @return bool
     */
    public function isVip() : bool
    {
        return in_array(self::ROLE_VIP, $this->roles);
    }
}