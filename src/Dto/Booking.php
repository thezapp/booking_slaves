<?php

namespace booking\Dto;

/**
 * Class Booking
 * @package booking\Dto
 */
class Booking
{
    /**
     * @var \DateTimeImmutable
     */
    public $dateFrom;

    /**
     * @var \DateTimeImmutable
     */
    public $dateTo;

    /**
     * @var integer
     */
    public $slaveId;

    /**
     * @var integer
     */
    public $tenantId;
}