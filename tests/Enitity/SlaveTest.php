<?php

namespace booking\Tests\Entity;

use booking\Entity\Period;
use booking\Entity\Slave\Profile;
use booking\Entity\Slave\Slave;
use booking\Entity\Tenant;

class SlaveTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $slave = $this->createSlave(
            new Profile(
                $nickname = 'nickname',
                $gender = 'male',
                $age = 20,
                $weight = 60,
                $birthPlace = 'Moscow',
                $characteristics = ['able to fish']
            ),
            $hourlyRate = 15, $cost = 2000
        );

        $this->assertEquals($nickname, $slave->getProfile()->getNickname());
        $this->assertEquals($gender, $slave->getProfile()->getGender());
        $this->assertEquals($age, $slave->getProfile()->getAge());
        $this->assertEquals($weight, $slave->getProfile()->getWeight());
        $this->assertEquals($birthPlace, $slave->getProfile()->getBirthplace());
        $this->assertEquals($characteristics, $slave->getProfile()->getCharacteristics());
        $this->assertEquals($hourlyRate, $slave->getHourlyRate());
        $this->assertEquals($cost, $slave->getCost());
    }

    public function testBookSuccess()
    {
        $slave = $this->createSlave();

        $tenant = new Tenant(['vip']);

        $slave->book($tenant, new Period($dateFrom = new \DateTimeImmutable('2019-10-11 11:30'), $dateTo = new \DateTimeImmutable('2019-10-11 10:30')));

        $this->assertNotEmpty($slave->getBookingList());

        $this->assertTrue($slave->getBookingList()[0]->isOpened());
        $this->assertEquals($dateTo->format('Y-m-d H:i:s'), $slave->getBookingList()[0]->getPeriod()->getDateTo()->format('Y-m-d H:i:s'));
        $this->assertEquals($dateFrom->format('Y-m-d H:i:s'), $slave->getBookingList()[0]->getPeriod()->getDateFrom()->format('Y-m-d H:i:s'));
        $this->assertTrue($slave->getBookingList()[0]->getTenant()->isVip());
    }

    public function testHoursLimitDateFromError()
    {
        $slave = $this->createSlave();

        $tenant = new Tenant(['vip']);

        $this->expectExceptionMessage(sprintf('Время аренды первого дня не может быть больше %d часов', Period::MAX_RENTAL_HOURS));

        $slave->book($tenant, new Period(new \DateTimeImmutable('2019-10-11 02:30'), new \DateTimeImmutable('2019-10-20 12:30')));
    }

    public function testHoursLimitDateToError()
    {
        $slave = $this->createSlave();

        $tenant = new Tenant(['vip']);

        $this->expectExceptionMessage(sprintf('Время аренды последнего дня не может быть больше %d часов', Period::MAX_RENTAL_HOURS));

        $slave->book($tenant, new Period(new \DateTimeImmutable('2019-10-11 09:30'), new \DateTimeImmutable('2019-10-20 22:00')));
    }

    public function testIsAlreadyBookedError()
    {
        $slave = $this->createSlave();

        $tenant = new Tenant(['vip']);

        $slave->book($tenant, new Period(new \DateTimeImmutable('2019-10-11 09:30'), new \DateTimeImmutable('2019-10-11 12:30')));

        $this->expectExceptionMessage('Раб на указанное время уже арендован');

        $slave->book($tenant, new Period(new \DateTimeImmutable('2019-10-11 10:30'), new \DateTimeImmutable('2019-10-11 11:30')));
    }

    public function testBookVipSuccess()
    {
        $slave = $this->createSlave();

        $tenant = new Tenant(['vip']);

        $slave->book(new Tenant(['user']), new Period(new \DateTimeImmutable('2019-10-11 11:00'), new \DateTimeImmutable('2019-10-11 17:30')));
        $slave->bookByVip($tenant, new Period($dateTo = new \DateTimeImmutable('2019-10-11 10:30'), $dateFrom = new \DateTimeImmutable('2019-10-11 12:30')));

        $this->assertNotEmpty($slave->getBookingList());
        $this->assertCount(2, $slave->getBookingList());

        $this->assertTrue($slave->getBookingList()[0]->isCanceled());
        $this->assertTrue($slave->getBookingList()[1]->isOpened());
        $this->assertEquals($slave->getBookingList()[0]->getReason(), 'Одна из дат аренды была отдана в пользу VIP-пользователя');
    }

    public function testBookVipIsNotVipTenantError()
    {
        $slave = $this->createSlave();

        $tenant = new Tenant(['user']);

        $this->expectExceptionMessage('Пользователь не является VIP-пользователем');

        $slave->bookByVip($tenant, new Period($dateTo = new \DateTimeImmutable('2019-10-11 10:30'), $dateFrom = new \DateTimeImmutable('2019-10-11 12:30')));
    }

    public function testBookVipBusyOtherVipError()
    {
        $slave = $this->createSlave();

        $this->expectExceptionMessage('Раб на указанное время уже арендован другим VIP-пользователем');

        $slave->bookByVip(new Tenant(['vip']), new Period($dateTo = new \DateTimeImmutable('2019-10-11 10:30'), $dateFrom = new \DateTimeImmutable('2019-10-11 12:30')));
        $slave->bookByVip(new Tenant(['vip']), new Period($dateTo = new \DateTimeImmutable('2019-10-11 09:30'), $dateFrom = new \DateTimeImmutable('2019-10-11 13:30')));
    }

    /**
     * @param null $profile
     * @param int $hourlyRate
     * @param int $cost
     * @return Slave
     */
    protected function createSlave($profile = null, $hourlyRate = 15, $cost = 2000)
    {
        if (!$profile){
            $profile = new Profile(
                'nickname',
                'male',
                20,
                60,
                'Moscow',
                ['able to fish']
            );
        }

        return new Slave($profile, $hourlyRate, $cost);
    }
}