--делал под Mysql, в PostgreSql тоже должно взлететь

--Получить минимальную, максимальную и среднюю стоимость всех рабов весом более 60 кг
SELECT MAX(cost), MIN(cost), AVG(cost) FROM slave WHERE weight > 60;

--Выбрать категории, в которых больше 10 рабов.
SELECT c.*
FROM slave s
JOIN category c ON c.id = s.category_id
GROUP BY s.id
HAVING count(s.id) > 10;

--Выбрать категорию с наибольшей суммарной стоимостью рабов.
SELECT SUM(cost) as sum
FROM slave s
JOIN category c ON c.id = s.category_id
GROUP by s.category_id
ORDER BY sum DESC
LIMIT 1 --по идее еще можно через ALL

--Выбрать категории, в которых мужчин больше чем женщин.
SELECT c.*, count(case when gender='m' then 1 end) as count_m, count(case when gender='m' then 1 end) as count_f,
FROM slave s
JOIN category c ON c.id = s.category_id
WHERE count_m > count_f
GROUP BY s.category_id

--Количество рабов в категории "Для кухни" (включая все вложенные категории).
--для категорий тут будет nested sets, можно mp или с parent_id(выборка поддерева будет сложнее)
SELECT count()
FROM slave s
JOIN category c ON c.id = s.category_id
WHERE c.name == 'Для кухни' AND (s.category_id BETWEEN c.lft AND c.rgt OR s.category_id = c.id)